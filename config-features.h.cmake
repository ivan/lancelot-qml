#ifndef CONFIG_FEATURES_H_
#define CONFIG_FEATURES_H_

#cmakedefine01 HAVE_NEPOMUK
#cmakedefine01 HAVE_TELEPATHY
#cmakedefine01 HAVE_KDEPIM

#cmakedefine01 HAVE_CXX11_AUTO
#cmakedefine01 HAVE_CXX11_NULLPTR
#cmakedefine01 HAVE_CXX11_LAMBDA
#cmakedefine01 HAVE_CXX11_OVERRIDE
#cmakedefine01 HAVE_CXX_OVERRIDE_ATTR

#define LANCELOT_PACKAGE_DIR QString::fromAscii("@DATA_INSTALL_DIR@/plasma/plasmoids/org.kde.lancelot/")

#endif
