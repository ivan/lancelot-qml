/*   vim:set foldmethod=marker:
 *
 *   Copyright (C) 2013 Ivan Cukic <ivan.cukic(at)kde.org>
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License version 2,
 *   or (at your option) any later version, as published by the Free
 *   Software Foundation
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details
 *
 *   You should have received a copy of the GNU General Public
 *   License along with this program; if not, write to the
 *   Free Software Foundation, Inc.,
 *   51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

import QtQuick 1.1

import org.kde.lancelot.models 0.1 as LancelotModels
import org.kde.lancelot.components 0.1 as LancelotComponents

LancelotComponents.ListView {
    id: main

    width:  100

    Behavior on x      { NumberAnimation { duration: 200 } }
    Behavior on y      { NumberAnimation { duration: 200 } }
    Behavior on width  { NumberAnimation { duration: 200 } }
    Behavior on height { NumberAnimation { duration: 200 } }
}

