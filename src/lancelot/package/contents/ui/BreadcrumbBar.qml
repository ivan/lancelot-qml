/*   vim:set foldmethod=marker:
 *
 *   Copyright (C) 2013 Ivan Cukic <ivan.cukic(at)kde.org>
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License version 2,
 *   or (at your option) any later version, as published by the Free
 *   Software Foundation
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details
 *
 *   You should have received a copy of the GNU General Public
 *   License along with this program; if not, write to the
 *   Free Software Foundation, Inc.,
 *   51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

import QtQuick 1.1

import org.kde.plasma.core 0.1 as PlasmaCore
import org.kde.plasma.components 0.1 as PlasmaComponents

Item {
    id: main

    /* property declarations --------------------------{{{ */
    /* }}} */

    /* signal declarations ----------------------------{{{ */
    signal popped
    /* }}} */

    /* JavaScript functions ---------------------------{{{ */
    function push(title) {
        main.state = parseInt(main.state) + 1

        if (main.state == "1") {
            button3.text = title

        } else if (main.state == "2") {
            button4.text = title

        }
    }

    function pop() {
        main.state = parseInt(main.state) - 1
    }
    /* }}} */

    /* object properties ------------------------------{{{ */
    width:  parent.width
    height: button1.height + background.margins.top  + background.margins.bottom
    /* }}} */

    /* child objects ----------------------------------{{{ */
    PlasmaCore.FrameSvgItem {
        id: background

        anchors.fill: parent
        imagePath: "lancelot/action-list-view-headers"

        height: parent.height
        width:  parent.width
    }

    Row {
        anchors.fill: parent

        BreadcrumbBarButton {
            id: button1

            text: "Favorites"

            onClicked: {
                if (main.state == "1") {
                    main.popped()
                } else if (main.state == "2") {
                    main.popped()
                    main.popped()
                }
            }

            enabled: false
        }

        BreadcrumbBarButton {
            id: button2

            text: "Applications"

            onClicked: {
                if (main.state == "2") {
                    main.popped()

                }
            }

            enabled: false
        }

        BreadcrumbBarButton {
            id: button3

            text: "Hello3"
            visible: width == 0 ? false : true

            enabled: false
        }

        BreadcrumbBarButton {
            id: button4

            text: "Hello4"
            visible: width == 0 ? false : true

            enabled: false
        }
    }
    /* }}} */

    /* states -----------------------------------------{{{ */
    state: "0"

    states: [
        State {
            name: "0"
            PropertyChanges { target: button1; width: main.width / 2 }
            PropertyChanges { target: button2; width: main.width / 2 }
            PropertyChanges { target: button3; width: 0 }
            PropertyChanges { target: button4; width: 0 }

            PropertyChanges { target: button1; enabled: false }
            PropertyChanges { target: button2; enabled: false }
        },
        State {
            name: "1"
            PropertyChanges { target: button1; width: 1 * main.width / 3 }
            PropertyChanges { target: button2; width: 1 * main.width / 3 }
            PropertyChanges { target: button3; width: 1 * main.width / 3 }
            PropertyChanges { target: button4; width: 0 }

            PropertyChanges { target: button1; enabled: true }
            PropertyChanges { target: button2; enabled: false }
        },
        State {
            name: "2"
            PropertyChanges { target: button1; width: 1 * main.width / 4 }
            PropertyChanges { target: button2; width: 1 * main.width / 4 }
            PropertyChanges { target: button3; width: 1 * main.width / 4 }
            PropertyChanges { target: button4; width: 1 * main.width / 4 }

            PropertyChanges { target: button1; enabled: true }
            PropertyChanges { target: button2; enabled: true }
        }
    ]
    /* }}} */

    /* transitions ------------------------------------{{{ */
    /* }}} */
}

