/*   vim:set foldmethod=marker:
 *
 *   Copyright (C) 2013 Ivan Cukic <ivan.cukic(at)kde.org>
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License version 2,
 *   or (at your option) any later version, as published by the Free
 *   Software Foundation
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details
 *
 *   You should have received a copy of the GNU General Public
 *   License along with this program; if not, write to the
 *   Free Software Foundation, Inc.,
 *   51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

import QtQuick 1.1
import org.kde.plasma.core 0.1 as PlasmaCore
import org.kde.plasma.components 0.1 as PlasmaComponents
import org.kde.qtextracomponents 0.1

import org.kde.lancelot.models 0.1 as LancelotModels
import org.kde.lancelot.components 0.1 as LancelotComponents

Item {
    id: main

    /* property declarations --------------------------{{{ */
    /* }}} */

    /* signal declarations ----------------------------{{{ */
    /* }}} */

    /* JavaScript functions ---------------------------{{{ */
    /* }}} */

    /* object properties ------------------------------{{{ */
    /* }}} */

    /* child objects ----------------------------------{{{ */
    PassagewayView {
        id: applicationsView

        anchors.fill: parent

        modelsList: [
            LancelotModels.FavoriteApplications { selfTitle: "" },
            LancelotModels.Applications { selfTitle: "" }
        ]

        onActivated: {
            if (action[0] == '/') return

            var modelLevel = (action.split("/").length - 1)
            var viewLevel  = parseInt(applicationsView.state)

            while (modelLevel <= viewLevel) {
                applicationsView.pop()
                viewLevel --;
            }

            var model = Qt.createQmlObject(
                'import org.kde.lancelot.models 0.1 as LancelotModels;\
                 LancelotModels.Applications { root: "' + action + '" }',
                applicationsView, 'model'
            );

            applicationsView.push(action, model)
        }
    }
    /* }}} */

    /* states -----------------------------------------{{{ */
    /* }}} */

    /* transitions ------------------------------------{{{ */
    /* }}} */
}

