/*   vim:set foldmethod=marker:
 *
 *   Copyright (C) 2013 Ivan Cukic <ivan.cukic(at)kde.org>
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License version 2,
 *   or (at your option) any later version, as published by the Free
 *   Software Foundation
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details
 *
 *   You should have received a copy of the GNU General Public
 *   License along with this program; if not, write to the
 *   Free Software Foundation, Inc.,
 *   51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

import QtQuick 1.1

Item {
    id: main

    /* property declarations --------------------------{{{ */
    property string currentPage: ""
    property string searchString: ""
    // property list<PageInfo> pages
    /* }}} */

    /* signal declarations ----------------------------{{{ */
    /* }}} */

    /* JavaScript functions ---------------------------{{{ */
    onCurrentPageChanged: {
    }

    onSearchStringChanged: {
        if (searchString == "") {
            currentPage = "applications";
            state = "applications";

        } else {
            currentPage = "search";
            state = "search";
            pageSearch.searchString = main.searchString
        }
    }
    /* }}} */

    /* object properties ------------------------------{{{ */
    /* }}} */

    /* child objects ----------------------------------{{{ */
    DocumentsPanel {
        id: pageDocuments

        anchors.fill: parent

        Behavior on opacity { NumberAnimation { duration: 100 } }
    }

    ContactsPanel {
        id: pageContacts

        anchors.fill: parent

        Behavior on opacity { NumberAnimation { duration: 100 } }
    }

    ComputerPanel {
        id: pageComputer

        anchors.fill: parent

        Behavior on opacity { NumberAnimation { duration: 100 } }
    }

    ApplicationsPanel {
        id: pageApplications

        anchors.fill: parent

        Behavior on opacity { NumberAnimation { duration: 100 } }
    }

    SearchPanel {
        id: pageSearch

        anchors.fill: parent

        Behavior on opacity { NumberAnimation { duration: 100 } }
    }
    /* }}} */

    /* states -----------------------------------------{{{ */
    states: [
         State {
             name: "documents"
             PropertyChanges { target: pageDocuments;    opacity: 1 }
             PropertyChanges { target: pageContacts;     opacity: 0 }
             PropertyChanges { target: pageComputer;     opacity: 0 }
             PropertyChanges { target: pageApplications; opacity: 0 }
             PropertyChanges { target: pageSearch;       opacity: 0 }
         },
         State {
             name: "contacts"
             PropertyChanges { target: pageDocuments;    opacity: 0 }
             PropertyChanges { target: pageContacts;     opacity: 1 }
             PropertyChanges { target: pageComputer;     opacity: 0 }
             PropertyChanges { target: pageApplications; opacity: 0 }
             PropertyChanges { target: pageSearch;       opacity: 0 }
         },
         State {
             name: "computer"
             PropertyChanges { target: pageDocuments;    opacity: 0 }
             PropertyChanges { target: pageContacts;     opacity: 0 }
             PropertyChanges { target: pageComputer;     opacity: 1 }
             PropertyChanges { target: pageApplications; opacity: 0 }
             PropertyChanges { target: pageSearch;       opacity: 0 }
         },
         State {
             name: "applications"
             PropertyChanges { target: pageDocuments;    opacity: 0 }
             PropertyChanges { target: pageContacts;     opacity: 0 }
             PropertyChanges { target: pageComputer;     opacity: 0 }
             PropertyChanges { target: pageApplications; opacity: 1 }
             PropertyChanges { target: pageSearch;       opacity: 0 }
         },
         State {
             name: "search"
             PropertyChanges { target: pageDocuments;    opacity: 0 }
             PropertyChanges { target: pageContacts;     opacity: 0 }
             PropertyChanges { target: pageComputer;     opacity: 0 }
             PropertyChanges { target: pageApplications; opacity: 0 }
             PropertyChanges { target: pageSearch;       opacity: 1 }
         }
     ]
    /* }}} */

    /* transitions ------------------------------------{{{ */
    /* }}} */
}

