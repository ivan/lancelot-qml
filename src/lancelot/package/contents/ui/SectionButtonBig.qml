/*   vim:set foldenable foldmethod=marker:
 *
 *   Copyright (C) 2012 Ivan Cukic <ivan.cukic(at)kde.org>
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License version 2,
 *   or (at your option) any later version, as published by the Free
 *   Software Foundation
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details
 *
 *   You should have received a copy of the GNU General Public
 *   License along with this program; if not, write to the
 *   Free Software Foundation, Inc.,
 *   51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

import QtQuick 1.1

import org.kde.plasma.core 0.1 as PlasmaCore
import org.kde.plasma.components 0.1 as PlasmaComponents

PlasmaCore.FrameSvgItem {
    id: root

    property bool checked: false

    property alias title: labelTitle.text
    property alias icon: icon.source

    imagePath: "lancelot/section-buttons"

    prefix: {
        if (checked) {
            "down"
        } else {
            if (mouse.containsMouse) {
                "active"
            } else {
                "normal"
            }
        }
    }

    Item {
        y: (parent.height - height) / 2

        anchors {
            centerIn: parent.center
        }

        width: parent.width
        height: icon.height + labelTitle.height

        PlasmaCore.IconItem {
            id: icon

            width: 48
            height: 48

            anchors {
                horizontalCenter: parent.horizontalCenter
            }
        }

        PlasmaComponents.Label {
            id: labelTitle

            horizontalAlignment: Text.AlignHCenter

            anchors {
                left: parent.left
                right: parent.right

                top: icon.bottom
            }
        }
    }

    MouseArea {
        id: mouse

        anchors.fill: parent

        hoverEnabled: true

        /*onEntered: root.prefix = "active"*/
        /*onExited:  root.prefix = "normal"*/
        /*onClicked: root.prefix = "down"*/

    }
}

