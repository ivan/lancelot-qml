/*   vim:set foldenable foldmethod=marker:
 *
 *   Copyright (C) 2012 Ivan Cukic <ivan.cukic(at)kde.org>
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License version 2,
 *   or (at your option) any later version, as published by the Free
 *   Software Foundation
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details
 *
 *   You should have received a copy of the GNU General Public
 *   License along with this program; if not, write to the
 *   Free Software Foundation, Inc.,
 *   51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

import QtQuick 1.1

import org.kde.plasma.core 0.1 as PlasmaCore
import org.kde.plasma.components 0.1 as PlasmaComponents

PlasmaCore.FrameSvgItem {
    id: main

    /* property declarations --------------------------{{{ */
    property bool checked: false

    property alias title: labelTitle.text
    property alias icon:  icon.source
    /* }}} */

    /* signal declarations ----------------------------{{{ */
    signal clicked
    /* }}} */

    /* JavaScript functions ---------------------------{{{ */
    /* }}} */

    /* object properties ------------------------------{{{ */
    imagePath: "lancelot/section-buttons"

    prefix: checked             ? "down"   :
            mouse.containsMouse ? "active" :
            /* other */           "normal"
    /* }}} */

    /* child objects ----------------------------------{{{ */
    PlasmaCore.IconItem {
        id: icon

        width:  main.width / 2
        height: main.width / 2

        anchors {
            horizontalCenter: parent.horizontalCenter
            bottom: parent.bottom

            topMargin:    8
            bottomMargin: 8
        }
    }

    PlasmaComponents.Label {
        id: labelTitle

        elide:  Text.ElideRight
        height: main.width / 2
        width:  parent.height - main.width

        anchors {
            horizontalCenter: parent.horizontalCenter

            bottom: icon.top
            top:    parent.top

            topMargin:    8
            bottomMargin: 8
        }

        transform: Rotation {
            origin.x: labelTitle.width / 2
            origin.y: labelTitle.height / 2
            angle:    -90
        }
    }

    MouseArea {
        id: mouse

        anchors.fill: parent

        hoverEnabled: true

        onEntered: timer.start()
        onExited:  timer.stop()
    }

    Timer {
        id: timer

        interval: 300
        running:  false
        repeat:   false

        onTriggered: main.clicked()
    }
    /* }}} */

    /* states -----------------------------------------{{{ */
    /* }}} */

    /* transitions ------------------------------------{{{ */
    /* }}} */
}

