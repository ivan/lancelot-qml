/*   vim:set foldenable foldmethod=marker:
 *
 *   Copyright (C) 2012 Ivan Cukic <ivan.cukic(at)kde.org>
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License version 2,
 *   or (at your option) any later version, as published by the Free
 *   Software Foundation
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details
 *
 *   You should have received a copy of the GNU General Public
 *   License along with this program; if not, write to the
 *   Free Software Foundation, Inc.,
 *   51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

import QtQuick 1.1

import org.kde.plasma.components 0.1 as PlasmaComponents

Item {
    id: root

    width: 300
    height: 300

    property int minimumWidth:  320
    property int minimumHeight: 400

    PlasmaComponents.ToolBar {
        id: toolbar

        anchors {
            top:   parent.top
            left:  parent.left
            right: parent.right
        }

        height: barSearch.height + margins.top + margins.bottom
    }

    SearchBar {
        id: barSearch

        anchors {
            top:   parent.top
            left:  panelMain.left
            right: parent.right

            topMargin:   toolbar.margins.top
            rightMargin: toolbar.margins.right
        }
    }

    SectionBar {
        id: barSections

        selectedSection: panelMain.state

        anchors {
            top:    toolbar.bottom
            left:   parent.left
            bottom: parent.bottom

            topMargin: 1
        }

        onClicked:  panelMain.state = name
    }

    Pages {
        id: panelMain

        state: "applications"
        searchString: barSearch.searchString

        anchors {
            top:    toolbar.bottom
            left:   barSections.right
            right:  parent.right
            bottom: parent.bottom

            topMargin: 1
        }
    }
}
