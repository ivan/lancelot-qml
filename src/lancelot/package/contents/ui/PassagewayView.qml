/*   vim:set foldmethod=marker:
 *
 *   Copyright (C) 2013 Ivan Cukic <ivan.cukic(at)kde.org>
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License version 2,
 *   or (at your option) any later version, as published by the Free
 *   Software Foundation
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details
 *
 *   You should have received a copy of the GNU General Public
 *   License along with this program; if not, write to the
 *   Free Software Foundation, Inc.,
 *   51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

import QtQuick 1.1

import org.kde.plasma.core 0.1 as PlasmaCore

import org.kde.lancelot.models 0.1 as LancelotModels
import org.kde.lancelot.components 0.1 as LancelotComponents

Item {
    id: main

    /* property declarations --------------------------{{{ */
    property list<QtObject> modelsList
    /* }}} */

    /* signal declarations ----------------------------{{{ */
    signal activated(variant action)
    /* }}} */

    /* JavaScript functions ---------------------------{{{ */
    function push(title, model) {
        main.state = parseInt(main.state) + 1

        title = title.substring(0, title.length - 1)

        var index = title.indexOf('/')

        if (index > 0) {
            title = title.substring(index + 1)
        }

        breadcrumb.push(title)

        if (main.state == "1") {
            list3.modelsList = [model]

        } else if (main.state == "2") {
            list4.modelsList = [model]

        }
    }

    function pop() {
        main.state = parseInt(main.state) - 1

        breadcrumb.pop()
    }
    /* }}} */

    /* object properties ------------------------------{{{ */
    /* }}} */

    /* child objects ----------------------------------{{{ */
    BreadcrumbBar {
        id: breadcrumb

        anchors {
            top:   parent.top
            left:  parent.left
            right: parent.right
        }

        onPopped: main.pop()
    }

    Row {
        anchors {
            top:    breadcrumb.bottom
            bottom: parent.bottom
            left:   parent.left
            right:  parent.right
        }

        AnimatedListView {
            id: list1

            height: parent.height
            showHeader: false

            modelsList: [ main.modelsList[0] ]
        }

        AnimatedListView {
            id: list2

            height: parent.height
            showHeader: false

            modelsList: [ main.modelsList[1] ]

            onActivated: main.activated(action)
        }

        AnimatedListView {
            id: list3

            height: parent.height
            showHeader: false

            onActivated: main.activated(action)
        }

        AnimatedListView {
            id: list4

            height: parent.height
            showHeader: false
        }
    }
    /* }}} */

    /* states -----------------------------------------{{{ */
    state: "0"

    states: [
        State {
            name: "0"
            PropertyChanges { target: list1; width: main.width / 2 }
            PropertyChanges { target: list2; width: main.width / 2 }
            PropertyChanges { target: list3; width: 0 }
            PropertyChanges { target: list4; width: 0 }
        },
        State {
            name: "1"
            PropertyChanges { target: list1; width: 0 }
            PropertyChanges { target: list2; width:     main.width / 3 }
            PropertyChanges { target: list3; width: 2 * main.width / 3 }
            PropertyChanges { target: list4; width: 0 }
        },
        State {
            name: "2"
            PropertyChanges { target: list1; width: 0 }
            PropertyChanges { target: list2; width: 1 * main.width / 6 }
            PropertyChanges { target: list3; width: 2 * main.width / 6 }
            PropertyChanges { target: list4; width: 3 * main.width / 6 }
        }
    ]
    /* }}} */

    /* transitions ------------------------------------{{{ */
    /* }}} */
}

