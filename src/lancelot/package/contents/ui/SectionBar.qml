/*   vim:set foldmethod=marker:
 *
 *   Copyright (C) 2013 Ivan Cukic <ivan.cukic(at)kde.org>
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License version 2,
 *   or (at your option) any later version, as published by the Free
 *   Software Foundation
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details
 *
 *   You should have received a copy of the GNU General Public
 *   License along with this program; if not, write to the
 *   Free Software Foundation, Inc.,
 *   51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

import QtQuick 1.1

import org.kde.plasma.core 0.1 as PlasmaCore
import org.kde.plasma.components 0.1 as PlasmaComponents

PlasmaCore.FrameSvgItem {
    id: main

    /* property declarations --------------------------{{{ */
    property string selectedSection
    /* }}} */

    /* signal declarations ----------------------------{{{ */
    signal clicked(string name)
    /* }}} */

    /* JavaScript functions ---------------------------{{{ */
    /* }}} */

    /* object properties ------------------------------{{{ */
    width: 64
    imagePath: "widgets/frame"
    enabledBorders: PlasmaCore.FrameSvg.RightBorder
    // combination of TopBorder | BottomBorder | LeftBorder | RightBorder
    /* }}} */

    /* child objects ----------------------------------{{{ */
    Column {
        anchors.fill: parent

        Repeater {
            id: repeaterSections

            model: ListModel {
                ListElement {
                    name:  "documents"
                    title: "Documents"
                    icon:  "applications-office"
                }
                ListElement {
                    name:  "contacts"
                    title: "Contacts"
                    icon:  "kontact"
                }
                ListElement {
                    name:  "computer"
                    title: "Computer"
                    icon:  "computer-laptop"
                }
                ListElement {
                    name:  "applications"
                    title: "Applications"
                    icon:  "kde"
                }
            }

            SectionButton {
                width:   parent.width
                height:  parent.height / repeaterSections.model.count

                icon:    model.icon
                title:   model.title

                // checked: (model.name == main.selectedSection)

                onClicked: {
                    main.clicked(model.name)
                    highlight.y = y
                    highlight.x = x
                    highlight.height = height
                    highlight.width  = width
                }
            }
        }
    }

    PlasmaCore.FrameSvgItem {
        id: highlight

        imagePath: "lancelot/section-buttons"
        prefix:    "down"

        Behavior on x      { NumberAnimation { duration: 200 } }
        Behavior on y      { NumberAnimation { duration: 200 } }
        Behavior on width  { NumberAnimation { duration: 200 } }
        Behavior on height { NumberAnimation { duration: 200 } }
    }
    /* }}} */

    /* states -----------------------------------------{{{ */
    /* }}} */

    /* transitions ------------------------------------{{{ */
    /* }}} */
}

