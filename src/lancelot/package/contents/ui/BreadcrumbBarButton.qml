/*   vim:set foldmethod=marker:
 *
 *   Copyright (C) 2013 Ivan Cukic <ivan.cukic(at)kde.org>
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License version 2,
 *   or (at your option) any later version, as published by the Free
 *   Software Foundation
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details
 *
 *   You should have received a copy of the GNU General Public
 *   License along with this program; if not, write to the
 *   Free Software Foundation, Inc.,
 *   51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

import QtQuick 1.1

import org.kde.plasma.core 0.1 as PlasmaCore
import org.kde.plasma.components 0.1 as PlasmaComponents

Item {
    id: main

    /* property declarations --------------------------{{{ */
    property alias text: label.text
    property alias backgroundImage: background.imagePath
    // property boolean enabled: true
    /* }}} */

    /* signal declarations ----------------------------{{{ */
    signal clicked
    /* }}} */

    /* JavaScript functions ---------------------------{{{ */
    /* }}} */

    /* object properties ------------------------------{{{ */
    height: label.height
    /* }}} */

    /* child objects ----------------------------------{{{ */
    PlasmaCore.FrameSvgItem {
        id: background

        anchors.fill: parent

        z: -1

        imagePath:
            main.enabled ?
                "lancelot/passageway-view-buttons" :
                "lancelot/passageway-view-inactivebuttons"
    }

    PlasmaComponents.Label {
        id: label

        anchors {
            fill: parent
            leftMargin: background.margins.left + 8
        }

        opacity: mouseArea.containsMouse ? 1 : 0.5
    }

    MouseArea {
        id: mouseArea

        enabled: main.enabled

        anchors.fill: parent
        onClicked: main.clicked()

        hoverEnabled: true
    }
    /* }}} */

    /* states -----------------------------------------{{{ */
    /* }}} */

    /* transitions ------------------------------------{{{ */
    /* }}} */
}

