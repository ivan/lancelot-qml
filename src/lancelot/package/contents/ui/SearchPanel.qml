/*   vim:set foldmethod=marker:
 *
 *   Copyright (C) 2013 Ivan Cukic <ivan.cukic(at)kde.org>
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License version 2,
 *   or (at your option) any later version, as published by the Free
 *   Software Foundation
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details
 *
 *   You should have received a copy of the GNU General Public
 *   License along with this program; if not, write to the
 *   Free Software Foundation, Inc.,
 *   51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

import QtQuick 1.1
import org.kde.plasma.core 0.1 as PlasmaCore
import org.kde.plasma.components 0.1 as PlasmaComponents
import org.kde.qtextracomponents 0.1

import org.kde.lancelot.models 0.1 as LancelotModels
import org.kde.lancelot.components 0.1 as LancelotComponents

import "search"

Item {
    id: main

    /* property declarations --------------------------{{{ */
    property string searchString: ""
    property string searchOfflineString: ""
    property string searchOnlineString: ""
    /* }}} */

    /* signal declarations ----------------------------{{{ */
    /* }}} */

    /* JavaScript functions ---------------------------{{{ */
    onSearchStringChanged: {
        delay.stop();

        if (searchString != "") {
            delay.start();
        }
    }
    /* }}} */

    /* object properties ------------------------------{{{ */
    Timer {
        id: delay

        interval: 500
        repeat:   false
        running:  false

        onTriggered: {

            if (main.searchString.indexOf("!web ") == 0) {
                main.searchOnlineString = main.searchString.substring(4);
                main.state = "online";

            } else {
                if (main.searchOfflineString.indexOf("!web ") == 0)
                    main.state = "offline";
                main.searchOnlineString = main.searchString;
            }

            main.searchOfflineString = main.searchString
        }
    }
    /* }}} */

    /* child objects ----------------------------------{{{ */
    Row {
        anchors.fill: parent

        LancelotComponents.ListView {
            id: searchOffline

            height: parent.height
            width:  parent.width - buttonOnline.width

            showHeader: false

            modelsList: [
                LancelotModels.Runner {
                    id: modelSearch
                    selfTitle: ""
                    searchString: main.searchOfflineString
                }
            ]

            Behavior on width  { NumberAnimation { duration: 200 } }
        }

        Item {
            id: buttonOnline

            width:  48
            height: parent.height

            Rectangle {
                color: labelOnline.color
                opacity: mouseOnline.containsMouse ? .2 : .1
                anchors.fill: parent

                Behavior on opacity  { NumberAnimation { duration: 200 } }
            }

            PlasmaCore.IconItem {
                y: parent.height - 40
                x: 8

                width: 32
                height: 32

                source: "applications-internet"
            }

            PlasmaComponents.Label {
                id: labelOnline

                text: "Search online"

                anchors.fill: parent

                transform: Rotation { origin.x: 24; origin.y: height/2; angle: -90 }
            }

            MouseArea {
                id: mouseOnline

                anchors.fill: parent
                hoverEnabled: true

                onClicked: main.state = (main.state == "offline") ? "online" : "offline"
            }
        }

        Item {
            id: searchOnline
            /*flickableDirection: Flickable.VerticalFlick*/

            property string searchString: ""

            height: parent.height
            width:  parent.width / 2

            Rectangle {
                color: labelOnline.color
                opacity: .1
                anchors.fill: parent
            }

            Column {

                spacing: 8

                width: parent.width
                height: parent.height

                DuckDuckGoInfo {
                    id: searchDDG

                    searchString: searchOnline.searchString
                    width:  parent.width
                }
            }

            Behavior on width  { NumberAnimation { duration: 200 } }
        }
    }
    /* }}} */

    /* states -----------------------------------------{{{ */
    state: "offline"

    states: [
        State {
            name: "offline"
            PropertyChanges { target: searchOffline; width: main.width - buttonOnline.width }
            PropertyChanges { target: searchOnline;  searchString: "" }
        },
        State {
            name: "online"
            PropertyChanges { target: searchOffline; width: main.width / 2 - buttonOnline.width }
            PropertyChanges { target: searchOnline;  searchString: main.searchOnlineString }
        }
    ]

    /* }}} */

    /* transitions ------------------------------------{{{ */
    /* }}} */
}

