/*   vim:set foldenable foldmethod=marker:
 *
 *   Copyright (C) 2012 Ivan Cukic <ivan.cukic(at)kde.org>
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License version 2,
 *   or (at your option) any later version, as published by the Free
 *   Software Foundation
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details
 *
 *   You should have received a copy of the GNU General Public
 *   License along with this program; if not, write to the
 *   Free Software Foundation, Inc.,
 *   51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

import QtQuick 1.1
import org.kde.plasma.core 0.1 as PlasmaCore
import org.kde.plasma.components 0.1 as PlasmaComponents
import org.kde.qtextracomponents 0.1

import org.kde.lancelot.models 0.1 as LancelotModels
import org.kde.lancelot.components 0.1 as LancelotComponents

import "DuckDuckGoInfo.js" as DDG
import "../"

Column {
    id: main

    /* property declarations --------------------------{{{ */
    property string searchString: ""
    /* }}} */

    /* signal declarations ----------------------------{{{ */
    /* }}} */

    /* JavaScript functions ---------------------------{{{ */
    onSearchStringChanged: {
        DDG.loadResults(main.searchString, model)
    }
    /* }}} */

    /* object properties ------------------------------{{{ */
    height: childrenRect.height
    /* }}} */

    /* child objects ----------------------------------{{{ */
    Item { width: parent.width; height: 8; visible: row.visible }

    Row {
        id: row

        width: parent.width
        visible: labelTitle.text != ""

        PlasmaCore.IconItem {
            id: iconItem

            width:  64
            height: 64

            source: "dialog-information"
        }

        Text {
            id: labelTitle
            color: "white"

            text:  model.selfTitle

            width: parent.width - 80
            wrapMode: Text.WordWrap
        }

        spacing: 8
    }

    spacing: 16

    LancelotComponents.List {
        id: listResults

        showHeader: false
        width:  parent.width

        model:  LancelotModels.BaseModel { id: model }
    }
    /* }}} */
}
