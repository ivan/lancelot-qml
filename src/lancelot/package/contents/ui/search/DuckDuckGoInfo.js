.pragma library

// Abstract: topic summary (can contain HTML, e.g. italics)
// AbstractText: topic summary (with no HTML)
// AbstractSource: name of Abstract source
// AbstractURL: deep link to expanded topic page in AbstractSource
// Image: link to image that goes with Abstract
// Heading: name of topic that goes with Abstract
//
// Answer: instant answer
// AnswerType: type of Answer, e.g. calc, color, digest, info, ip, iploc, phone, pw, rand, regexp, unicode, upc, or zip (see goodies & tech pages for examples).
//
// Definition: dictionary definition (may differ from Abstract)
// DefinitionSource: name of Definition source
// DefinitionURL: deep link to expanded definition page in DefinitionSource
//
// RelatedTopics: array of internal links to related topics associated with Abstract
//   Result: HTML link(s) to related topic(s)
//   FirstURL: first URL in Result
//   Icon: icon associated with related topic(s)
//     URL: URL of icon
//     Height: height of icon (px)
//     Width: width of icon (px)
//   Text: text from first URL
//
// Results: array of external links associated with Abstract
//   Result: HTML link(s) to external site(s)
//   FirstURL: first URL in Result
//   Icon: icon associated with FirstURL
//     URL: URL of icon
//     Height: height of icon (px)
//     Width: width of icon (px)
//   Text: text from FirstURL
//
// Type: response category, i.e. A (article), D (disambiguation), C (category), N (name), E (exclusive), or nothing.
//
// Redirect: !bang redirect URL
//
// format: output format (json or xml)
//
// If format=='json', you can also pass:
//
// callback: function to callback (JSONP format)
// pretty: 1 to make JSON look pretty (like JSONView for Chrome/Firefox)
//
// no_redirect: 1 to skip HTTP redirects (for !bang commands).
//
// no_html: 1 to remove HTML from text, e.g. bold and italics.
//
// skip_disambig: 1 to skip disambiguation (D) Type.

var loadResults = function (infoQuery, resultingModel) {
    if (infoQuery == "") return

    var request = new XMLHttpRequest()
    request.open("GET", "http://api.duckduckgo.com/?q=" + infoQuery + "&format=json&no_html=1&pretty=1", true)

    request.onreadystatechange = function() {
        if (request.readyState == request.DONE && request.status == 200) {
            var response = JSON.parse(request.responseText)

            var countResults = 0;

            if (response.Results.size == 0) return

            resultingModel.clear()

            var addItem = function (name, description, icon, action) {
                if (name && name != "" && countResults < 10) {
                    countResults++;

                    print("DDG: Adding item: " + name + " " + description + " " + action);

                    resultingModel.add(
                            name, description, icon, action
                        );
                }
            }

            // Processing Definition section

            if (response.Definition != "") {
                resultingModel.setSelfTitle(response.Definition)
            }

            // Processing results

            var addItems = function(items) {
                print("DDG: got:" + items + " " + items.length)

                for (id in items) {
                    var item = items[id]

                    var iconUrl = "applications-internet"

                    if (item.Icon && item.Icon.URL) {
                        iconUrl = item.Icon.URL
                    }

                    print("DDG: Found: " + item)
                    print("DDG: Icon: "  + iconUrl)

                    addItem(
                        item.Text,
                        item.FirstURL,
                        iconUrl,
                        item.FirstURL
                    )

                    if (item.Topics) {
                        addItems(item.Topics)
                    }
                }
            }

            resultingModel.setInhibitUpdateSignals(true);
            addItems(response.Results)
            addItems(response.RelatedTopics)
            resultingModel.setInhibitUpdateSignals(false);

        }
    }

    resultingModel.setSelfTitle("")

    request.send()
}

