/*
 *   Copyright (C) 2009, 2010, 2011, 2012, 2013 Ivan Cukic <ivan.cukic(at)kde.org>
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License version 2,
 *   or (at your option) any later version, as published by the Free
 *   Software Foundation
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details
 *
 *   You should have received a copy of the GNU General Public
 *   License along with this program; if not, write to the
 *   Free Software Foundation, Inc.,
 *   51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

#include "LancelotApplet.h"
// #include "Engine.h"

#include <QDeclarativeEngine>
#include <QDeclarativeContext>

#include <Plasma/DeclarativeWidget>
#include <KDesktopFile>
#include <KDebug>

#include <config-features.h>

class LancelotApplet::Private {
public:
    Plasma::DeclarativeWidget * root;
    KDesktopFile * desktop;
    // Engine * engine;

    bool initialized : 1;
};

LancelotApplet::LancelotApplet(QObject * parent, const QVariantList &args)
  : Plasma::PopupApplet(parent, args), d(new Private())
{
    kDebug() << "Location ###";

    d->initialized = false;
    // d->engine = new Engine(this);

    d->desktop = 0;
    d->root = 0;
}

LancelotApplet::~LancelotApplet()
{
    delete d->desktop;
    delete d->root;
    delete d;
}

void LancelotApplet::init()
{
    setPopupIcon("lancelot-start");

    if (d->initialized) return;


    d->root = new Plasma::DeclarativeWidget();
    d->desktop = new KDesktopFile(LANCELOT_PACKAGE_DIR + "metadata.desktop");

    setGraphicsWidget(d->root);
    d->root->setInitializationDelayed(true);
    // d->root->engine()->rootContext()->setContextProperty("engine", d->engine);

    d->root->setQmlPath(LANCELOT_PACKAGE_DIR + "/contents/" + d->desktop->desktopGroup().readEntry("X-Plasma-MainScript"));

    d->initialized = true;
}

void LancelotApplet::popupEvent(bool show)
{
    // d->engine->setPopupShown(show);
    Plasma::PopupApplet::popupEvent(show);
}

K_EXPORT_PLASMA_APPLET(lancelotapplet, LancelotApplet)

