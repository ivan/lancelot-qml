/*
 *   Copyright (C) 2012,  2013 Ivan Cukic <ivan.cukic(at)kde.org>
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License version 2,
 *   or (at your option) any later version, as published by the Free
 *   Software Foundation
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details
 *
 *   You should have received a copy of the GNU General Public
 *   License along with this program; if not, write to the
 *   Free Software Foundation,3 Inc.,
 *   51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

#ifndef UTILS_CONTAINS_H
#define UTILS_CONTAINS_H

#include <algorithm>

namespace lancelot {
namespace utils {

namespace details {

    template <typename Iterator, typename Function>
    bool any_of(Iterator start, Iterator end, Function f)
    {
        return (std::find_if(start, end, f) != end);
    }

    // Container functions

    template <typename Container, typename Function>
    bool _contains_helper_container(const Container & c, Function f,
            decltype(&Container::constBegin) * )
    {
        return any_of(c.constBegin(), c.constEnd(), f);
    }

    template <typename Container, typename Function>
    bool _contains_helper_container(const Container & c, Function f,
            decltype(&Container::cbegin) * )
    {
        return any_of(c.cbegin(), c.cend(), f);
    }

} // namespace details

template <typename Container, typename Function>
bool any_of(const Container & c, Function f)
{
    return details::_contains_helper_container
        <Container, Function>(c, f, nullptr);
}

} // namespace utils
} // namespace lancelot

#endif // UTILS_CONTAINS_H
