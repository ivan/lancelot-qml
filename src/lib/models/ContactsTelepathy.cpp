/*
 *   Copyright (C) 2007, 2008, 2009, 2010, 2011, 2012 Ivan Cukic <ivan.cukic(at)kde.org>
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU Lesser/Library General Public License version 2,
 *   or (at your option) any later version, as published by the Free
 *   Software Foundation
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU Lesser/Library General Public License for more details
 *
 *   You should have received a copy of the GNU Lesser/Library General Public
 *   License along with this program; if not, write to the
 *   Free Software Foundation, Inc.,
 *   51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

#include "ContactsTelepathy.h"

#include <TelepathyQt/PendingChannelRequest>
#include <TelepathyQt/PendingContacts>
#include <TelepathyQt/ContactManager>
#include <TelepathyQt/PendingReady>
#include <TelepathyQt/Types>

#include <KTp/contact-factory.h>
#include <KTp/types.h>
#include <KTp/actions.h>
#include <KTp/Widgets/add-contact-dialog.h>
#include <KTp/Widgets/join-chat-room-dialog.h>
#include <KTp/Models/contacts-list-model.h>

#include <KDebug>

#include <utils/d_ptr_implementation.h>
#include <utils/val.h>

class ContactsTelepathy::Private {
public:
};

class ContactsListModel: public KTp::ContactsListModel {
public:
    explicit ContactsListModel(QObject *parent = 0)
      : KTp::ContactsListModel(parent)
    {
        QHash <int, QByteArray> roleNames;
        roleNames[Qt::DisplayRole]         = "name";
        roleNames[Qt::StatusTipRole]       = "description";
        roleNames[Qt::DecorationRole]      = "icon";
        roleNames[Qt::UserRole]            = "action";
        setRoleNames(roleNames);
    }
};

ContactsTelepathy::ContactsTelepathy()
    : d()
{
    Tp::registerTypes();
    // Tp::enableDebug(KCmdLineArgs::parsedArgs()->isSet("debug"));
    // Tp::enableWarnings(true);

    Tp::AccountFactoryPtr  accountFactory = Tp::AccountFactory::create(QDBusConnection::sessionBus(),
        Tp::Features() << Tp::Account::FeatureCore
        << Tp::Account::FeatureAvatar
        << Tp::Account::FeatureCapabilities
        << Tp::Account::FeatureProtocolInfo
        << Tp::Account::FeatureProfile);

    Tp::ConnectionFactoryPtr connectionFactory = Tp::ConnectionFactory::create(QDBusConnection::sessionBus(),
        Tp::Features() << Tp::Connection::FeatureCore
        << Tp::Connection::FeatureRosterGroups
        << Tp::Connection::FeatureRoster
        << Tp::Connection::FeatureSelfContact);

    Tp::ContactFactoryPtr contactFactory = KTp::ContactFactory::create(Tp::Features()  << Tp::Contact::FeatureAlias
        << Tp::Contact::FeatureAvatarData
        << Tp::Contact::FeatureSimplePresence
        << Tp::Contact::FeatureCapabilities
        << Tp::Contact::FeatureClientTypes);

    Tp::ChannelFactoryPtr channelFactory = Tp::ChannelFactory::create(QDBusConnection::sessionBus());

    val accountManager = Tp::AccountManager::create(QDBusConnection::sessionBus(),
        accountFactory,
        connectionFactory,
        channelFactory,
        contactFactory
    );

    val source = new ContactsListModel(this);
    source->setAccountManager(accountManager);
    setSourceModel(source);

    setPresenceTypeFilterFlags(KTp::ContactsFilterModel::ShowOnlyConnected);

    QHash <int, QByteArray> roleNames;
    roleNames[Qt::DisplayRole]         = "name";
    roleNames[Qt::StatusTipRole]       = "description";
    roleNames[Qt::DecorationRole]      = "icon";
    roleNames[Qt::UserRole]            = "action";
    setRoleNames(roleNames);
}

ContactsTelepathy::~ContactsTelepathy()
{
}

QVariant ContactsTelepathy::data(const QModelIndex &index, int role) const
{
    // if (role == Qt::DecorationRole) {
    //     kDebug() << "Avatar:" << data(index, KTp::ContactAvatarPathRole);
    //     kDebug() << "Presence:" << data(index, KTp::ContactPresenceIconRole);
    // }

    return
        role == Qt::DecorationRole ?
            KIcon([=] () -> QString {
                val icon = data(index, KTp::ContactAvatarPathRole).toString();
                return !icon.isEmpty() ? icon : data(index,  KTp::ContactPresenceIconRole).toString();
            } ()) :

        role == Qt::UserRole ? // index.row() : // data(index,  KTp::ContactRole) :
            (QList<QVariant>()
                << data(index, KTp::ContactRole)
                << data(index, KTp::AccountRole)
            ) :

        /* default */
            KTp::ContactsFilterModel::data(index, role);
}

void ContactsTelepathy::activate(const QVariant & action)
{
    val data = action.toList();
    val contact = data.at(0).value<KTp::ContactPtr>();
    val account = data.at(1).value<Tp::AccountPtr>();

    val operation = KTp::Actions::startChat(account, contact, true);

    connect(operation, SIGNAL(finished(Tp::PendingOperation*)),
                       SLOT(startChatFinished(Tp::PendingOperation*))
           );
}

void ContactsTelepathy::startChatFinished(Tp::PendingOperation * operation)
{
    // TODO: Handle errors somehow
    // kDebug() << "ERROR:"
    //          << operation->isError()
    //          << operation->errorName()
    //          << operation->errorMessage();
}

QString ContactsTelepathy::selfTitle() const
{
    return i18n("Online contacts");
}

QIcon ContactsTelepathy::selfIcon() const
{
    return KIcon("telepathy-kde");
}

#include "ContactsTelepathy.moc"
