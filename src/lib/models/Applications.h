/*
 *   Copyright (C) 2007, 2008, 2009, 2010, 2011, 2012, 2013 Ivan Cukic <ivan.cukic(at)kde.org>
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU Lesser/Library General Public License version 2,
 *   or (at your option) any later version, as published by the Free
 *   Software Foundation
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU Lesser/Library General Public License for more details
 *
 *   You should have received a copy of the GNU Lesser/Library General Public
 *   License along with this program; if not, write to the
 *   Free Software Foundation, Inc.,
 *   51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

#ifndef LANCELOTAPP_MODELS_APPLICATIONS_H
#define LANCELOTAPP_MODELS_APPLICATIONS_H

#include <lancelot/lancelot_export.h>

#include "BaseModel.h"

#include <utils/d_ptr.h>

/**
 * Applications data model.
 */
class Applications : public BaseModel {
    Q_OBJECT

    Q_PROPERTY(QString root READ root WRITE setRoot)

public:
    /**
     * Creates a new instance of Applications
     * @param root category to show applications for
     * @param title the title of the data model
     * @param icon the icon of the data model
     * @param flat if false, subcategories will behave like normal items
     */
    explicit Applications(QString root = "", QString title = "", QIcon icon = QIcon(), bool flat = false);

    /**
     * Destroys this Applications instance
     */
    virtual ~Applications();

    QString selfTitle() const;
    QIcon   selfIcon()  const;

    QString root() const;
    void setRoot(const QString & root);

protected:
    void load() override;

public Q_SLOTS:
    virtual void activate(const QVariant & action);

private:
    D_PTR;
};

#endif /* LANCELOTAPP_MODELS_APPLICATIONS_H */
