/*
 *   Copyright (C) 2007, 2008, 2009, 2010, 2011, 2012 Ivan Cukic <ivan.cukic(at)kde.org>
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU Lesser/Library General Public License version 2,
 *   or (at your option) any later version, as published by the Free
 *   Software Foundation
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU Lesser/Library General Public License for more details
 *
 *   You should have received a copy of the GNU Lesser/Library General Public
 *   License along with this program; if not, write to the
 *   Free Software Foundation, Inc.,
 *   51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

#include "BaseModel.h"

#include <QFileInfo>
#include <QApplication>

#include <KRun>
#include <KLocalizedString>
#include <KDesktopFile>
#include <KDebug>
#include <KFileItem>
#include <KIcon>
#include <KGlobal>
#include <KMimeType>
#include <KUrl>

#include <algorithm>

#include <utils/d_ptr_implementation.h>
#include <utils/any_of.h>

#include <utils/val.h>

class BaseModel::Private {
public:
    Private()
    {
    }

    QString title;
    QIcon icon;
    QList < BaseModel::Item > items;
    bool sendEmits : 1;
    bool hasLingeringEmits : 1;
};

BaseModel::BaseModel(const QString & title, const QIcon & icon)
    : d()
{
    d->title = title;
    d->icon  = icon;

    d->sendEmits = true;
    d->hasLingeringEmits = false;

    QHash <int, QByteArray> roleNames;
    roleNames[Qt::DisplayRole]    = "name";
    roleNames[Qt::StatusTipRole]  = "description";
    roleNames[Qt::DecorationRole] = "icon";
    roleNames[Qt::UserRole]       = "action";
    setRoleNames(roleNames);
}

BaseModel::~BaseModel()
{
}

void BaseModel::activate(const QVariant & action)
{
    new KRun(KUrl(action.toString()), 0);
}

int BaseModel::rowCount(const QModelIndex & parent) const
{
    Q_UNUSED(parent);

    return d->items.count();
}

int BaseModel::count() const
{
    return d->items.count();
}

QVariant BaseModel::data(const QModelIndex & index, int role) const
{
    val row = index.row();

    if (row >= d->items.count() || row < 0) {
        return QVariant();
    }

    val item = d->items.at(row);

    switch (role) {
        case Qt::DisplayRole:
            return item.title;

        case Qt::StatusTipRole:
            return item.description;

        case Qt::DecorationRole:
            return item.icon;

        case Qt::UserRole:
            return item.data;

        default:
            return QVariant();
    }
}

//

void BaseModel::add(const Item & item)
{
    beginInsertRows(index(0, 0), d->items.count(), d->items.count());
    d->items.append(item);
    endInsertRows();

    emitUpdateSignal();
}

void BaseModel::add(const QString & title, const QString & description, const QString & icon, const QVariant & data)
{
    add(Item(title, description, icon, data));
}

void BaseModel::add(const QString & title, const QString & description, QIcon icon, const QVariant & data)
{
    add(Item(title, description, icon, data));
}

void BaseModel::insert(int where, const Item & item)
{
    beginInsertRows(index(0, 0), where, where);
    d->items.insert(where, item);
    endInsertRows();

    emitUpdateSignal();
}

void BaseModel::insert(int where, const QString & title, const QString & description, QIcon icon, const QVariant & data)
{
    insert(where, Item(title, description, icon, data));
}

void BaseModel::set(int where, const Item & item)
{
    if (where < 0 || where >= d->items.count()) return;

    d->items[where] = item;

    emit dataChanged(index(where), index(where));
}

void BaseModel::set(int where, const QString & title, const QString & description, QIcon icon, const QVariant & data)
{
    set(where, Item(title, description, icon, data));
}

void BaseModel::removeAt(int where)
{
    beginRemoveRows(index(0, 0), where, where);
    d->items.removeAt(where);
    endRemoveRows();

    emitUpdateSignal();
}

const BaseModel::Item & BaseModel::itemAt(int index)
{
    return d->items[index];
}

void BaseModel::clear()
{
    beginResetModel();
    d->items.clear();
    endResetModel();

    emitUpdateSignal();
}


/**
 * The services can be supplied with a list of alternative ones
 * separated by pipe (|). countForServices executes the specified
 * method for all service alternatives until it returns true.
 * This is done individually for every service in the list.
 */

template <typename F>
static int countForServices(const QStringList & serviceNames, F method)
{
    using std::count_if;
    using lancelot::utils::any_of;

    return count_if(serviceNames.constBegin(), serviceNames.constEnd(),

        [method] (const QString & _)
        {
            return any_of(
                _.split('|'), method
            );
        }
    );
}

int BaseModel::addServices(const QStringList & serviceNames)
{
    return countForServices(
        serviceNames,
        [this] (const QString & _) { return addService(_); }
    );
}

bool BaseModel::addService(const QString & serviceName)
{
    val service = KService::serviceByStorageId(serviceName);

    return addService(service);
}

bool BaseModel::addService(const KService::Ptr & service)
{
    if (!service || !service->isValid()) {
        return false;
    }

    val genericName = service->genericName();
    val appName = service->name();

    add(
        genericName.isEmpty() ? appName : genericName,
        genericName.isEmpty() ? "" : appName,
        KIcon(service->icon()),
        service->entryPath()
    );

    return true;
}

int BaseModel::addUrls(const QStringList & urls)
{
    return std::count_if(urls.constBegin(), urls.constEnd(),
        [this] (const QString & _) { return addUrl(_); }
    );
}

bool BaseModel::addUrl(const QString & url)
{
    return addUrl(KUrl(url));
}

bool BaseModel::addUrl(const KUrl & url)
{
    // kDebug() << url;
    KFileItem fileItem(KFileItem::Unknown, KFileItem::Unknown, url);

    if (url.isLocalFile() && QFileInfo(url.path()).suffix() == "desktop") {
        // .desktop files may be services (type field == 'Application' or 'Service')
        // or they may be other types such as links.
        //
        // first look in the KDE service database to see if this file is a service,
        // otherwise represent it as a generic .desktop file

        KDesktopFile desktopFile(url.path());

        if ((desktopFile.readType() == "Service" || desktopFile.readType() == "Application")
                && addService(url.path())) {
            return true;
        }

        KUrl desktopUrl(desktopFile.readUrl());

        add(
            QFileInfo(url.path()).baseName(),
            desktopUrl.isLocalFile() ? desktopUrl.path() : desktopUrl.prettyUrl(),
            KIcon(desktopFile.readIcon()),
            // url.path() //desktopFile.readUrl()
            url.url()
        );

    } else {
        add(
            fileItem.text(),
            url.isLocalFile() ? url.path() : url.prettyUrl(),
            KIcon(fileItem.iconName()),
            url.url()
        );

    }

    return true;
}

// inserts
int BaseModel::insertServices(int where, const QStringList & serviceNames)
{
    return countForServices(
        serviceNames, // TODO reverse()
        [this,  where] (const QString & _) { return insertService(where, _); }
    );
}

bool BaseModel::insertService(int where, const QString & serviceName)
{
    const KService::Ptr service = KService::serviceByStorageId(serviceName);
    return insertService(where, service);
}

bool BaseModel::insertService(int where, const KService::Ptr & service)
{
    if (!service) {
        return false;
    }

    QString genericName = service->genericName();
    QString appName = service->name();

    insert(
        where,
        genericName.isEmpty() ? appName : genericName,
        genericName.isEmpty() ? "" : appName,
        KIcon(service->icon()),
        service->entryPath()
    );

    return true;
}

int BaseModel::insertUrls(int where, const QStringList & urls)
{
    return std::count_if(urls.constBegin(), urls.constEnd(),
        [this, where] (const QString & _) { return insertUrl(where, _); }
    );
}

bool BaseModel::insertUrl(int where, const QString & url)
{
    const KUrl kurl(url);
    return insertUrl(where, kurl);
}

bool BaseModel::insertUrl(int where, const KUrl & url)
{
    KFileItem fileItem(KFileItem::Unknown, KFileItem::Unknown, url);

    if (url.isLocalFile() && QFileInfo(url.path()).suffix() == "desktop") {
        // .desktop files may be services (type field == 'Application' or 'Service')
        // or they may be other types such as links.
        //
        // first look in the KDE service database to see if this file is a service,
        // otherwise represent it as a generic .desktop file

        KDesktopFile desktopFile(url.path());

        if ((desktopFile.readType() == "Service" || desktopFile.readType() == "Application")
                && insertService(where, url.path())) {
            return true;
        }

        KUrl desktopUrl(desktopFile.readUrl());

        insert(
            where,
            QFileInfo(url.path()).baseName(),
            desktopUrl.isLocalFile() ? desktopUrl.path() : desktopUrl.prettyUrl(),
            KIcon(desktopFile.readIcon()),
            // url.path() //desktopFile.readUrl()
            url.url()
        );

    } else {
        insert(
            where,
            fileItem.text(),
            url.isLocalFile() ? url.path() : url.prettyUrl(),
            KIcon(fileItem.iconName()),
            url.url()
        );

    }

    return true;
}

QString BaseModel::selfTitle() const
{
    return d->title;
}

QIcon BaseModel::selfIcon() const
{
    return d->icon;
}

void BaseModel::setSelfTitle(const QString & title)
{
    d->title = title;
    emit selfTitleChanged();
}

void BaseModel::setSelfIcon(const QIcon & icon)
{
    d->icon = icon;
    emit selfIconChanged();
}

void BaseModel::emitUpdateSignal()
{
    if (d->sendEmits) {
        emit countChanged();
        reset();

    } else {
        d->hasLingeringEmits = true;

    }
}

void BaseModel::setInhibitUpdateSignals(bool value)
{
    d->sendEmits = !value;

    if (d->hasLingeringEmits) {
        emitUpdateSignal();
        d->hasLingeringEmits = false;
    }
}

void BaseModel::load()
{
}

#include "BaseModel.moc"

