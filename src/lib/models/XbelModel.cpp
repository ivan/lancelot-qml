/*
 *   Copyright (C) 2007, 2008, 2009, 2010, 2011, 2012 Ivan Cukic <ivan.cukic(at)kde.org>
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU Lesser/Library General Public License version 2,
 *   or (at your option) any later version, as published by the Free
 *   Software Foundation
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU Lesser/Library General Public License for more details
 *
 *   You should have received a copy of the GNU Lesser/Library General Public
 *   License along with this program; if not, write to the
 *   Free Software Foundation, Inc.,
 *   51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

#include "XbelModel.h"
#include <KIcon>
#include <KDebug>

#include <QFile>
#include <QXmlStreamReader>

#include <utils/d_ptr_implementation.h>

class XbelModel::Private {
public:
    Private(XbelModel * parent)
        : q(parent)
    {}

    void readXbel();
    void readFolder();
    void readBookmark();

    QString filePath;
    QXmlStreamReader xmlReader;

    XbelModel * const q;

};

XbelModel::XbelModel(const QString & file)
    : BaseModel(), d(this)
{
    // kDebug() << "Constructing XbelModel";
    d->filePath = file;
    load();
}

XbelModel::~XbelModel()
{
}

QString XbelModel::file() const
{
    return d->filePath;
}

void XbelModel::setFile(const QString & file)
{
    if (d->filePath == file) return;

    d->filePath = file;

    reload();
}


void XbelModel::reload()
{
    clear();
    load();
}

static inline bool isStartElement(const QXmlStreamReader & xml, const QString & tagName)
{
    return (xml.isStartElement() && xml.name() == tagName);
}

static inline bool isEndElement(const QXmlStreamReader & xml, const QString & tagName)
{
    return (xml.isEndElement() && xml.name() == tagName);
}

static inline bool tagMatchesButNotContents(QXmlStreamReader & xml, const QString & tagName, const QString & tagText)
{
    return (
        xml.isStartElement() &&
        xml.name() == tagName &&
        xml.readElementText() != tagText
    );
}

static inline bool tagMatches(QXmlStreamReader & xml, const QString & tagName, const QString & tagText)
{
    return (
        xml.isStartElement() &&
        xml.name() == tagName &&
        xml.readElementText() == tagText
    );
}

void XbelModel::load()
{
    // kDebug() << "Loading...";

    if (d->filePath.isEmpty()) return;

    QFile file(d->filePath);

    if (!file.open(QIODevice::ReadOnly | QIODevice::Text)) {
        // kDebug() << "File can't be opened:" << d->filePath;
        return;
    }

    // kDebug() << "Opened" << file.fileName();
    d->xmlReader.setDevice(& file);

    while (!d->xmlReader.atEnd()) {
        d->xmlReader.readNext();

        if (isStartElement(d->xmlReader, "xbel")) {
            d->readXbel();
        }
    }
}

void XbelModel::Private::readXbel()
{
    // kDebug() << "Found the Xbel tag";

    while (!xmlReader.atEnd()) {
        xmlReader.readNext();

        if (isEndElement(xmlReader, "xbel")) break;

        if (isStartElement(xmlReader, "folder")) {
            readFolder();

        } else if (isStartElement(xmlReader, "bookmark")) {
            readBookmark();

        }
    }
}

void XbelModel::Private::readFolder()
{
    // kDebug() << "Ignoring the contents of a folder";

    while (!xmlReader.atEnd()) {
        xmlReader.readNext();

        if (isEndElement(xmlReader, "folder")) break;
    }
}

void XbelModel::Private::readBookmark()
{
    // kDebug() << "Reading a bookmark";

    Item bookmarkItem;
    bool showBookmark = true;

    KUrl url(xmlReader.attributes().value("href").toString());

    bookmarkItem.data = url.url();

    bookmarkItem.description =
        url.isLocalFile() ? url.path() : url.url();

    while (!xmlReader.atEnd()) {
        xmlReader.readNext();

        if (isEndElement(xmlReader, "bookmark")) break;

        if (isStartElement(xmlReader, "title")) {
            bookmarkItem.title = xmlReader.readElementText();
            // kDebug() << "Got title:" << bookmarkItem.title;

        } else if (isStartElement(xmlReader, "icon")) {
            QString icon = xmlReader.attributes().value("name").toString();
            if (!icon.isEmpty()) {
                bookmarkItem.icon = KIcon(icon);
            }
            // kDebug() << "Got icon:" << bookmarkItem.icon;

        } else if (tagMatchesButNotContents(xmlReader, "IsHidden", "false")) {
            // kDebug() << "Hiding the bookmark - IsHidden is true";
            showBookmark = false;

        } else if (tagMatchesButNotContents(xmlReader, "OnlyInApp", "lancelot")) {
            // kDebug() << "Hiding the item that shouldn't be in lancelot";
            showBookmark = false;

        } else if (tagMatches(xmlReader, "isSystemItem", "true")) {
            // kDebug() << "This is a system item, i18n-ing it";
            bookmarkItem.title = i18n(bookmarkItem.title.toUtf8().data());

        }
    }

    if (showBookmark) {
        q->add(bookmarkItem);
    }
}

#include "XbelModel.moc"
