/*
 *   Copyright (C) 2007, 2008, 2009, 2010, 2011, 2012 Ivan Cukic <ivan.cukic(at)kde.org>
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU Lesser/Library General Public License version 2,
 *   or (at your option) any later version, as published by the Free
 *   Software Foundation
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU Lesser/Library General Public License for more details
 *
 *   You should have received a copy of the GNU Lesser/Library General Public
 *   License along with this program; if not, write to the
 *   Free Software Foundation, Inc.,
 *   51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

#include "Folder.h"

#include <KConfig>
#include <KConfigGroup>
#include <KDebug>
#include <KDirLister>
#include <KFileItem>
#include <KIcon>
#include <KProtocolManager>
#include <KRun>
#include <KStandardDirs>

#include <QDir>
#include <QFileInfo>

#include <utils/d_ptr_implementation.h>
#include <utils/val.h>

class Folder::Private {
public:
    Private(Folder * parent);

    class Folder * const q;
    KDirModel * model;
};

Folder::Private::Private(Folder * parent)
    : q(parent)
{
}

Folder::Folder()
    : d(this)
{
    d->model = new KDirModel(this);
    setSourceModel(d->model);
    setSortFoldersFirst(true);

    QHash <int, QByteArray> roleNames;
    roleNames[Qt::DisplayRole]         = "name";
    roleNames[Qt::StatusTipRole]       = "description";
    roleNames[Qt::DecorationRole]      = "icon";
    roleNames[KDirModel::FileItemRole] = "action";
    setRoleNames(roleNames);

    // for (int i = 0; i < d->model->columnCount(); i++) {
    //     kDebug() << "Header" << d->model->headerData(i, Qt::Horizontal);
    // }

    setPath(QDir::homePath());
}

Folder::~Folder()
{
}

QVariant Folder::data(const QModelIndex &index, int role) const
{
    if (role == KDirModel::FileItemRole) {
        val fileItem = QSortFilterProxyModel::data(index, role).value<KFileItem>();
        return fileItem;

    } else {
        return KDirSortFilterProxyModel::data(index, role);

    }
}

QString Folder::selfTitle() const
{
    return d->model->dirLister()->url().fileName();
}

QIcon Folder::selfIcon() const
{
    return KIcon("folder");
}

QString Folder::path() const
{
    return d->model->dirLister()->url().url();
}

void Folder::setPath(const QString & path)
{
    d->model->dirLister()->openUrl(KUrl(path), KDirLister::Keep);
}

void Folder::activate(const QVariant & file)
{
    file.value<KFileItem>().run();
}

#include "Folder.moc"
