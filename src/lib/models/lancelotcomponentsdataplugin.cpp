/*
 *   Copyright (C) 2011, 2012 Ivan Cukic <ivan.cukic(at)kde.org>
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU Library General Public License as
 *   published by the Free Software Foundation; either version 2, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details
 *
 *   You should have received a copy of the GNU Library General Public
 *   License along with this program; if not, write to the
 *   Free Software Foundation, Inc.,
 *   51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

#include "lancelotcomponentsdataplugin.h"

#include <QtDeclarative/qdeclarative.h>

#include <config-features.h>

#include "AvailableModels.h"
#include "Applications.h"
#include "ContactsTelepathy.h"
#include "Devices.h"
#include "FavoriteApplications.h"
#include "Folder.h"
#include "MessagesAkonadi.h"
#include "NewDocuments.h"
#include "Places.h"
#include "Runner.h"
#include "SystemServices.h"
#include "XbelModel.h"

#define QML_REGISTER_TYPE(Type)                                     \
    qmlRegisterType < Type > (uri, 0, 1, #Type);

#define REGISTER_MODEL(Title, Icon, Type)                           \
    QML_REGISTER_TYPE(Type);                                        \
    AvailableModels::addModel(Title, QIcon::fromTheme(Icon), #Type)


void LancelotComponentsDataPlugin::registerTypes(const char * uri)
{
    Q_ASSERT(uri == QLatin1String("org.kde.lancelot.models"));

    QML_REGISTER_TYPE(AvailableModels);
    QML_REGISTER_TYPE(BaseModel);
    QML_REGISTER_TYPE(Runner);
    // QML_REGISTER_TYPE(ContactsKopete);
    // QML_REGISTER_TYPE(Folder);
    // QML_REGISTER_TYPE(BasicFolderModel);
    // QML_REGISTER_TYPE(Devices);
    // QML_REGISTER_TYPE(FavoriteApplications);
    // QML_REGISTER_TYPE(MessagesAkonadi);
    // QML_REGISTER_TYPE(NewDocuments);
    // QML_REGISTER_TYPE(OpenDocuments);
    // QML_REGISTER_TYPE(Places);
    // QML_REGISTER_TYPE(RecentDocuments);
    // QML_REGISTER_TYPE(SystemServices);

    REGISTER_MODEL(i18n("Favorite applications"), "favorites", FavoriteApplications);
    REGISTER_MODEL(i18n("Folder..."), "folder", Folder);
    REGISTER_MODEL(i18n("Devices"), "media-optical", Devices);
    REGISTER_MODEL(i18n("Contacts"), "telepathy-kde", ContactsTelepathy);
    REGISTER_MODEL(i18n("Unread messages"), "kmail", MessagesAkonadi);
    REGISTER_MODEL(i18n("System tools"), "computer", SystemServices);
    REGISTER_MODEL(i18n("New documents"), "document-new", NewDocuments);
    REGISTER_MODEL(i18n("Bookmarks"), "bookmark", XbelModel);
    REGISTER_MODEL(i18n("Places"), "folder", Places);
    REGISTER_MODEL(i18n("Applications category..."), "plasmaapplet-shelf", Applications);
    // REGISTER_MODEL(i18n("Open documents"), "document-edit", OpenDocuments);
    // REGISTER_MODEL(i18n("Recent documents"), "document-open-recent", RecentDocuments);
    // REGISTER_MODEL(i18n("Online contacts"), "kopete", ContactsKopete);

    // Tree model:
    // QML_REGISTER_TYPE(SystemActions);
    // QML_REGISTER_TYPE(Applications);
}


#include "lancelotcomponentsdataplugin.moc"

