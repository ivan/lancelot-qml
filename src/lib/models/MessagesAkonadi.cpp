/*
 *   Copyright (C) 2007, 2008, 2009, 2010, 2011, 2012 Ivan Cukic <ivan.cukic(at)kde.org>
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU Lesser/Library General Public License version 2,
 *   or (at your option) any later version, as published by the Free
 *   Software Foundation
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU Lesser/Library General Public License for more details
 *
 *   You should have received a copy of the GNU Lesser/Library General Public
 *   License along with this program; if not, write to the
 *   Free Software Foundation, Inc.,
 *   51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

#include "MessagesAkonadi.h"

#include <QDBusConnection>
#include <QDBusInterface>
#include <QDBusConnectionInterface>

#include <KIcon>
#include <KRun>
#include <KToolInvocation>
#include <KStandardDirs>

#include <utils/d_ptr_implementation.h>

// just in case messages:
#if 0
I18N_NOOP("Unread messages");
I18N_NOOP("Unable to find Kontact");
I18N_NOOP("Start Akonadi server");
I18N_NOOP("Akonadi server is not running");
#endif

// We have kdepimlibs

#include <KJob>
#include <Akonadi/Collection>
#include <Akonadi/CollectionFetchJob>
#include <Akonadi/CollectionStatistics>
#include <Akonadi/CollectionStatisticsJob>
#include <Akonadi/EntityDisplayAttribute>
#include "MessagesAkonadi_p.h"

#include <utils/nullptr.h>
#include <utils/val.h>

MessagesAkonadi::Private::Private(MessagesAkonadi * parent)
  : monitor(nullptr), unread(0), q(parent)
{
}

void MessagesAkonadi::Private::fetchEmailCollectionsDone(KJob * job)
{
    kDebug();

    q->clear();
    unread = 0;

    q->beginResetModel();
    // TODO: q->setEmitInhibited(true);

    if (job->error()) {
        kDebug() << "Job Error:" << job->errorString();

    } else {
        val cjob = static_cast<Akonadi::CollectionFetchJob *>(job);

        foreach (val collection, cjob->collections()) {
            if (collection.contentMimeTypes().contains("message/rfc822")) {
                val unreadCount = collection.statistics().unreadCount();

                if (unreadCount) {
                    kDebug()
                        << collection.name()
                        << unreadCount
                        << collection.url()
                        ;
                    q->add(
                            i18nc("Directory name (number of unread messages)",
                                "%1 (%2)",
                                collection.name(),
                                unreadCount),
                            QString::null,
                            entityIcon(collection),
                            collection.id()
                        );
                    unread += unreadCount;
                }
            }
        }
    }

    if (q->count() == 0) {
        q->add(i18n("No unread mail"), "", KIcon("mail-folder-inbox"), QVariant());
    }

    // if (unread) {
    //     q->setSelfTitle(i18nc("Unread messages (number of unread messages)",
    //                           "Unread messages (%1)", QString::number(unread)));

    // } else {
    //     q->setSelfTitle(i18n("Unread messages"));

    // }

    // TODO: q->setEmitInhibited(false);
    // TODO: q->updated();
    q->endResetModel();
}

KIcon MessagesAkonadi::Private::entityIcon(const Akonadi::Collection & collection) const
{
    val displayAttribute =
            collection.attribute<Akonadi::EntityDisplayAttribute>();

    return displayAttribute ?
        displayAttribute->icon() :
        KIcon("mail-folder-inbox");
}

MessagesAkonadi::MessagesAkonadi()
    : BaseModel(i18n("Unread messages"), KIcon("kmail")), d(this)
{
    val monitor = new Akonadi::Monitor();
    monitor->setCollectionMonitored(Akonadi::Collection::root());
    monitor->fetchCollection(true);

    connect(monitor, SIGNAL(collectionAdded(Akonadi::Collection,Akonadi::Collection)),
            this, SLOT(updateLater()));
    connect(monitor, SIGNAL(collectionRemoved(Akonadi::Collection)),
            this, SLOT(updateLater()));
    connect(monitor, SIGNAL(collectionChanged(Akonadi::Collection)),
            this, SLOT(updateLater()));
    connect(monitor, SIGNAL(collectionStatisticsChanged(Akonadi::Collection::Id,Akonadi::CollectionStatistics)),
            this, SLOT(updateLater()));

    connect(Akonadi::ServerManager::self(), SIGNAL(stateChanged(Akonadi::ServerManager::State)),
            this, SLOT(updateLater()));

    d->monitor.reset(monitor);

    load();
}

MessagesAkonadi::~MessagesAkonadi()
{
}

void MessagesAkonadi::updateLater()
{
    QTimer::singleShot(200, this, SLOT(update()));
}

void MessagesAkonadi::update()
{
    load();
}

void MessagesAkonadi::activate(const QVariant & action)
{
    Q_UNUSED(action);

    kDebug() << action.toInt();

    if (!QDBusConnection::sessionBus().interface()->isServiceRegistered("org.kde.kmail")) {
        KToolInvocation::startServiceByDesktopName("kmail2");
    }

    // TODO: This could be a bit prettier
    if (action.toString().isEmpty() && d->unread == 0) {
        if (!Akonadi::ServerManager::isRunning()) {
            Akonadi::ServerManager::start();
        }

        return;
    }

    QDBusInterface kmailInterface("org.kde.kmail", "/KMail", "org.kde.kmail.kmail");

    // Is the user running kmail or kontact?
    if (QDBusConnection::sessionBus().interface()->isServiceRegistered("org.kde.kontact")) {
        // Kontact, ok, lets raise it
        kDebug() << "Opening contact window";
        QDBusInterface kontactInterface("org.kde.kontact", "/KontactInterface", "org.kde.kontact.KontactInterface");
        QDBusInterface kontactWindowInterface("org.kde.kontact", "/kontact/MainWindow_1", "org.qtproject.Qt.QWidget");

        kontactInterface.call("selectPlugin", "kontact_kmailplugin");
        kontactWindowInterface.call("show");
        kontactWindowInterface.call("raise");
        kontactWindowInterface.call("setFocus");

    } else {
        // Just kmail
        kDebug() << "Opening kmail window";
        kmailInterface.call("openReader");

    }

    kmailInterface.call("showFolder", action.toString());
}

// QString MessagesAkonadi::selfShortTitle() const
// {
//     if (d->unread) {
//         return QString::number(d->unread);
//     } else {
//         return QString();
//     }
// }

void MessagesAkonadi::load()
{
    kDebug();

    if (!Akonadi::ServerManager::isRunning()) {
        clear();
        d->unread = 0;
        add(i18n("Start Akonadi server"), i18n("Akonadi server is not running"), KIcon("akonadi"), QVariant("start-akonadi"));
        return;

    }

    auto emailCollection = Akonadi::Collection::root();
    emailCollection.setContentMimeTypes(QStringList() << "message/rfc822");

    Akonadi::CollectionFetchScope scope;
    scope.setIncludeStatistics(true);
    scope.setContentMimeTypes(QStringList() << "message/rfc822");
    scope.setAncestorRetrieval(Akonadi::CollectionFetchScope::All);

    val fetch = new Akonadi::CollectionFetchJob(
            emailCollection, Akonadi::CollectionFetchJob::Recursive);
    fetch->setFetchScope(scope);

    connect(fetch, SIGNAL(result(KJob*)),
            d.get(), SLOT(fetchEmailCollectionsDone(KJob*)));
}

#include "MessagesAkonadi_p.moc"
#include "MessagesAkonadi.moc"
