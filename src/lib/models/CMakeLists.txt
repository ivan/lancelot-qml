project (lancelotmodels)

include (KDE4Defaults)

set (
    lancelotcomponentsdata_SRCS

    lancelotcomponentsdataplugin.cpp
    BaseModel.cpp
    AvailableModels.cpp

    Devices.cpp
    FavoriteApplications.cpp
    Folder.cpp
    Runner.cpp

    Applications.cpp
    SystemServices.cpp
    NewDocuments.cpp

    ContactsTelepathy.cpp
    MessagesAkonadi.cpp

    XbelModel.cpp
    Places.cpp
)

INCLUDE_DIRECTORIES (
    ${KDE4_INCLUDES}
    ${CMAKE_SOURCE_DIR}/
    ${CMAKE_BINARY_DIR}/
    ${CMAKE_SOURCE_DIR}/src
    ${CMAKE_BINARY_DIR}/src
    )

qt4_automoc (${lancelotcomponentsdata_SRCS})

#################################################

add_library (
    lancelotcomponentsdataplugin
    SHARED
    ${lancelotcomponentsdata_SRCS}
    )

target_link_libraries (
    lancelotcomponentsdataplugin
    ${QT_QTCORE_LIBRARY}
    ${QT_QTGUI_LIBRARY}
    ${QT_QTDECLARATIVE_LIBRARY}
    ${KDE4_KDEUI_LIBS}
    ${KDE4_KFILE_LIBS}
    ${KDE4_KIO_LIBS}
    ${KDE4_PLASMA_LIBS}
    ${KDE4_SOLID_LIBS}
    ${ADDITIONAL_LINK_LIBRARIES}
    ${KDE4WORKSPACE_TASKMANAGER_LIBRARY}
    )

install (
    TARGETS
    lancelotcomponentsdataplugin
    DESTINATION
    ${IMPORTS_INSTALL_DIR}/org/kde/lancelot/models
    )

install (
    FILES
    qmldir
    DESTINATION
    ${IMPORTS_INSTALL_DIR}/org/kde/lancelot/models
    )
