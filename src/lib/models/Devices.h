/*
 *   Copyright (C) 2007, 2008, 2009, 2010 Ivan Cukic <ivan.cukic(at)kde.org>
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU Lesser/Library General Public License version 2,
 *   or (at your option) any later version, as published by the Free
 *   Software Foundation
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU Lesser/Library General Public License for more details
 *
 *   You should have received a copy of the GNU Lesser/Library General Public
 *   License along with this program; if not, write to the
 *   Free Software Foundation, Inc.,
 *   51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

#ifndef LANCELOT_DATA_DEVICES_H
#define LANCELOT_DATA_DEVICES_H

#include "BaseModel.h"
#include <solid/device.h>
#include <solid/storageaccess.h>
#include <QXmlStreamReader>

#include <utils/d_ptr.h>

class Devices: public BaseModel {
    Q_OBJECT
    Q_CLASSINFO("DefaultProperty", "filter")

    Q_PROPERTY(Type filter READ filter WRITE setFilter NOTIFY filterChanged)
    Q_ENUMS(Type)

public:
    enum Type {
        Fixed = 1,
        Removable = 2,
        All = 0
    };

    Devices(Type filter = All);
    virtual ~Devices();

    void setFilter(Type type);
    Type filter() const;

Q_SIGNALS:
    void filterChanged();

public Q_SLOTS:
    Q_INVOKABLE void activate(const QVariant & udi) override;

protected:
    void load();

private:
    D_PTR;
};

#endif /* LANCELOT_DATA_DEVICES_H */
