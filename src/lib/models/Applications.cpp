/*
 *   Copyright (C) 2007, 2008, 2009, 2010, 2011, 2012, 2013 Ivan Cukic <ivan.cukic(at)kde.org>
 *   Copyright (C) 2007 Robert Knight <robertknight@gmail.com>
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU Lesser/Library General Public License version 2,
 *   or (at your option) any later version, as published by the Free
 *   Software Foundation
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU Lesser/Library General Public License for more details
 *
 *   You should have received a copy of the GNU Lesser/Library General Public
 *   License along with this program; if not, write to the
 *   Free Software Foundation, Inc.,
 *   51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

#include "Applications.h"
#include "Applications_p.h"

#include <KIcon>
#include <KRun>
#include <KServiceGroup>
#include <KStandardDirs>
#include <KSycoca>
#include <KUrl>
#include <KToolInvocation>
#include <KDebug>

#include "FavoriteApplications.h"

#include <utils/d_ptr_implementation.h>
#include <utils/val.h>

// Applications

// Applications::Private::Private(Applications * parent)
//     : q(parent)
// {
// }
//
// Applications::Private::~Private()
// {
// }
//
// void Applications::Private::sycocaUpdated(const QStringList & resources)
// {
//     // if (resources.contains("services") || resources.contains("apps")) {
//     //     load();
//     // }
// }

// void Applications::load()
// {
//     d->load();
// }

QString Applications::root() const
{
    return d->root;
}

void Applications::setRoot(const QString & root)
{
    d->root = root;

    clear();
    load();
}

void Applications::load()
{
    auto services = KServiceGroup::group(d->root);

    if (!services || !services->isValid()) return;

    if (d->title.isEmpty() || d->icon.isNull()) {
        d->title = services->caption();
        d->icon = KIcon(services->icon());
    }

    // KServiceGroup::List list = services->entries();
    val list = services->entries(
            true  /* sorted */,
            true  /* exclude no display entries */,
            false /* allow separators */,
            false /* sort by generic name */
        );

    // application name <-> service map for detecting duplicate entries

    QHash<QString, KService::Ptr> existingServices;

    for (auto it = list.constBegin(); it != list.constEnd(); ++it) {

        Item item;

        val p = (*it);

        if (p->isType(KST_KService)) {
            // This is a service/application

            val service = KService::Ptr::staticCast(p);

            if (service->noDisplay()) continue;

            item.icon = KIcon(service->icon());
            item.title = service->name();
            item.description = service->genericName();
            item.data = service->entryPath();

            add(item);

        } else if (p->isType(KST_KServiceGroup)) {
            // This is a subfolder/submenu

            val serviceGroup = KServiceGroup::Ptr::staticCast(p);

            if (serviceGroup->noDisplay() || serviceGroup->childCount() == 0) continue;

            item.icon = KIcon(serviceGroup->icon());
            item.title = serviceGroup->caption();
            item.description = QString();
            item.data = serviceGroup->relPath();

            add(item);
        }
    }
}

Applications::Applications(QString root, QString title, QIcon icon, bool flat)
    : d()
{
    d->root = root;
    d->title = title;
    d->icon = icon;

    // connect(KSycoca::self(), SIGNAL(databaseChanged(QStringList)),
    //         d, SLOT(sycocaUpdated(QStringList)));

    load();
}

Applications::~Applications()
{
}

void Applications::activate(const QVariant & action)
{
    BaseModel::activate(action);
}

QString Applications::selfTitle() const
{
    return d->title;
}

QIcon Applications::selfIcon() const
{
    return d->icon;
}

#include "Applications.moc"
