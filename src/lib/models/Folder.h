/*
 *   Copyright (C) 2007, 2008, 2009, 2010, 2011, 2012 Ivan Cukic <ivan.cukic(at)kde.org>
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU Lesser/Library General Public License version 2,
 *   or (at your option) any later version, as published by the Free
 *   Software Foundation
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU Lesser/Library General Public License for more details
 *
 *   You should have received a copy of the GNU Lesser/Library General Public
 *   License along with this program; if not, write to the
 *   Free Software Foundation, Inc.,
 *   51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

#ifndef LANCELOT_DATA_DIRMODEL_H
#define LANCELOT_DATA_DIRMODEL_H

#include "BaseModel.h"

#include <KDirModel>
#include <KDirSortFilterProxyModel>
#include <KUrl>

#include <utils/d_ptr.h>

class QVariant;

class Folder : public KDirSortFilterProxyModel {
    Q_OBJECT
    Q_CLASSINFO("DefaultProperty", "path")

    Q_PROPERTY(QString path READ path WRITE setPath)

    Q_PROPERTY(QString selfTitle  READ selfTitle  CONSTANT)
    Q_PROPERTY(QIcon   selfIcon   READ selfIcon   CONSTANT)

public:
    explicit Folder();
    virtual ~Folder();

    QString path() const;
    void setPath(const QString & path);

    virtual QVariant data(const QModelIndex & index, int role = Qt::DisplayRole) const override;

    QString selfTitle() const;
    QIcon selfIcon() const;

public Q_SLOTS:
    /**
     * Activates the specified item
     */
    virtual void activate(const QVariant & fileItem);

protected:
    void load();

private:
    D_PTR;
    // friend class FolderPrivate;
};

#endif /* LANCELOT_DATA_DIRMODEL_H */
