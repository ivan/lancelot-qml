/*
 *   Copyright (C) 2007, 2008, 2009, 2010 Ivan Cukic <ivan.cukic(at)kde.org>
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU Lesser/Library General Public License version 2,
 *   or (at your option) any later version, as published by the Free
 *   Software Foundation
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU Lesser/Library General Public License for more details
 *
 *   You should have received a copy of the GNU Lesser/Library General Public
 *   License along with this program; if not, write to the
 *   Free Software Foundation, Inc.,
 *   51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

#ifndef LANCELOT_DATA_MESSAGES_AKONADI_P_H
#define LANCELOT_DATA_MESSAGES_AKONADI_P_H

#include "MessagesAkonadi.h"
#include <QHash>
#include <KIcon>
#include <KJob>

#include <Akonadi/Entity>
#include <Akonadi/Monitor>
#include <Akonadi/Collection>
#include <Akonadi/CollectionFetchJob>
#include <Akonadi/CollectionFetchScope>
#include <Akonadi/ServerManager>

#include <memory>

class MessagesAkonadi::Private: public QObject {
    Q_OBJECT

public:
    Private(MessagesAkonadi * parent);

public Q_SLOTS:
    void fetchEmailCollectionsDone(KJob * job);

public:
    QHash < KJob *, Akonadi::Collection > collectionJobs;

    std::unique_ptr<Akonadi::Monitor> monitor;
    int unread;

    KIcon entityIcon(const Akonadi::Collection & collection) const;

private:
    MessagesAkonadi * const q;
};

#endif // LANCELOT_DATA_MESSAGESAKONADI_P_H

