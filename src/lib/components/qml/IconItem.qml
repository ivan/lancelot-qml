/*   vim:set foldmethod=marker:
 *
 *   Copyright (C) 2013 Ivan Cukic <ivan.cukic(at)kde.org>
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License version 2,
 *   or (at your option) any later version, as published by the Free
 *   Software Foundation
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details
 *
 *   You should have received a copy of the GNU General Public
 *   License along with this program; if not, write to the
 *   Free Software Foundation, Inc.,
 *   51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

import QtQuick 1.1

Item {
    id: main

    /* property declarations --------------------------{{{ */
    property int padding: 8

    property alias title:       content.title
    property alias description: content.description
    property alias iconSource:  content.iconSource

    property alias hovered:     mouseArea.containsMouse
    /* }}} */

    /* signal declarations ----------------------------{{{ */
    signal clicked
    /* }}} */

    /* JavaScript functions ---------------------------{{{ */
    /* }}} */

    /* object properties ------------------------------{{{ */
    width:  content.width  + 2 * padding
    height: content.height + 2 * padding
    /* }}} */

    /* child objects ----------------------------------{{{ */
    BasicVerticalContent {
        id: content

        iconSize: 48

        width: 96
        height: 96

        x: main.padding
        y: main.padding

        active: mouseArea.containsMouse
    }

    MouseArea {
        id: mouseArea

        anchors.fill: parent
        hoverEnabled: true

        onClicked: main.clicked()
    }
    /* }}} */

    /* states -----------------------------------------{{{ */
    /* }}} */

    /* transitions ------------------------------------{{{ */
    /* }}} */
}

