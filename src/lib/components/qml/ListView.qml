/*   vim:set foldmethod=marker:
 *
 *   Copyright (C) 2013 Ivan Cukic <ivan.cukic(at)kde.org>
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License version 2,
 *   or (at your option) any later version, as published by the Free
 *   Software Foundation
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details
 *
 *   You should have received a copy of the GNU General Public
 *   License along with this program; if not, write to the
 *   Free Software Foundation, Inc.,
 *   51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

import QtQuick 1.1

import org.kde.plasma.core 0.1 as PlasmaCore
import org.kde.plasma.components 0.1 as PlasmaComponents

Flickable {
    id: main

    /* property declarations --------------------------{{{ */
    property list<QtObject> modelsList
    property bool showHeader: true

    property Component delegate: ListItem {
            width: main.width

            title:       model.name
            description: model.description ? model.description : ""
            iconSource:  model.icon

            onClicked: {
                main.model.activate(model.action)
                main.activated(model.action)
            }

            onHoveredChanged: if (hovered) {
                highlight.x = x + parent.x
                highlight.y = y + parent.y
                highlight.height = height
            }
        }
    /* }}} */

    /* signal declarations ----------------------------{{{ */
    signal activated(variant action)
    /* }}} */

    /* JavaScript functions ---------------------------{{{ */
    onModelsListChanged: itemHighlight.height = 0
    /* }}} */

    /* object properties ------------------------------{{{ */
    clip: true
    flickableDirection: Flickable.VerticalFlick

    contentWidth:  content.width
    contentHeight: content.height
    /* }}} */

    /* child objects ----------------------------------{{{ */
    PlasmaComponents.Highlight {
        id: itemHighlight

        width:  main.width
        height: 0
        x:      0
        y:      0

        visible: (width * height > 0)

        Behavior on x      { NumberAnimation { duration: 200 } }
        Behavior on y      { NumberAnimation { duration: 200 } }
        Behavior on width  { NumberAnimation { duration: 100 } }
        Behavior on height { NumberAnimation { duration: 100 } }
    }

    Column {
        id: content

        Repeater {
            model: modelsList

            List {
                width:       main.width
                delegate:    main.delegate

                model:       modelsList[index]
                highlight:   itemHighlight

                showHeader:  main.showHeader

                title:       modelsList[index].selfTitle ? modelsList[index].selfTitle : ""

                onActivated: main.activated(action)
            }
        }
    }
    // }

    /* states -----------------------------------------{{{ */
    /* }}} */

    /* transitions ------------------------------------{{{ */
    /* }}} */
}

