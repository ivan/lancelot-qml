/*   vim:set foldmethod=marker:
 *
 *   Copyright (C) 2013 Ivan Cukic <ivan.cukic(at)kde.org>
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License version 2,
 *   or (at your option) any later version, as published by the Free
 *   Software Foundation
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details
 *
 *   You should have received a copy of the GNU General Public
 *   License along with this program; if not, write to the
 *   Free Software Foundation, Inc.,
 *   51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

import QtQuick 1.1

import org.kde.plasma.core 0.1 as PlasmaCore
import org.kde.plasma.components 0.1 as PlasmaComponents

Item {
    id: main

    property int step: 0

    signal activated

    anchors {
        right:  parent.right
        top:    parent.top
        bottom: parent.bottom
    }

    width:  22

    PlasmaCore.SvgItem {
        z: 1
        anchors.centerIn: parent

        height: parent.width
        width:  parent.width

        svg: PlasmaCore.Svg {
            id: svg

            imagePath: "lancelot/extender-button-icon"
        }

        elementId: "frame" + main.step
    }

    MouseArea {
        z: 2
        anchors.fill: parent

        hoverEnabled: true

        onEntered: timer.start()
        onExited:  { timer.stop(); main.step = 0; }
    }

    Timer {
        id: timer
        repeat: true
        interval: 500 / 8

        onTriggered: {
            main.step++

            if (main.step == 8)
                main.activated()

            if (main.step > 8)
                main.step = 8
        }
    }
}

