/*   vim:set foldmethod=marker:
 *
 *   Copyright (C) 2013 Ivan Cukic <ivan.cukic(at)kde.org>
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License version 2,
 *   or (at your option) any later version, as published by the Free
 *   Software Foundation
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details
 *
 *   You should have received a copy of the GNU General Public
 *   License along with this program; if not, write to the
 *   Free Software Foundation, Inc.,
 *   51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

import QtQuick 1.1

import org.kde.plasma.core 0.1 as PlasmaCore
import org.kde.plasma.components 0.1 as PlasmaComponents

Item {
    id: main

    /* property declarations --------------------------{{{ */
    property alias   title       : labelTitle.text
    property alias   description : labelDescription.text
    //property alias   iconSource  : iconItem.source
    property int     iconSize    : 32
    property bool    active      : false

    property variant iconSource
    /* }}} */

    /* signal declarations ----------------------------{{{ */
    /* }}} */

    /* JavaScript functions ---------------------------{{{ */
    onIconSourceChanged: {
        if (iconSource.toString().indexOf("http") == 0) {
            iconOnline.source = iconSource.toString();
            iconItem.visible = false;

        } else {
            iconItem.source = iconSource
            iconItem.visible = true;
        }

    }
    /* }}} */

    /* object properties ------------------------------{{{ */
    width:  parent ? parent.width : childrenRect.width
    height: childrenRect.height
    clip:   true
    /* }}} */

    /* child objects ----------------------------------{{{ */
    Row {
        id: row

        width: parent.width

        PlasmaCore.IconItem {
            id: iconItem

            width:  main.iconSize
            height: main.iconSize

            anchors.verticalCenter: parent.verticalCenter

            source: ""
        }

        Image {
            id: iconOnline

            width:  main.iconSize
            height: main.iconSize

            visible: !iconItem.visible

            source: ""
        }

        Column {
            width: row.width - main.iconSize - row.spacing

            PlasmaComponents.Label {
                id: labelTitle

                text: "Title"
                elide: Text.ElideRight

                width: parent.width
            }

            Text {
                id: labelDescription

                text: ""
                elide: Text.ElideRight

                font.pixelSize: labelTitle.font.pixelSize * 5 / 6
                color:          labelTitle.color
                opacity:        main.active ? 1 : 0.5

                width: parent.width
            }
        }

        spacing: 8
    }
    /* }}} */

    /* states -----------------------------------------{{{ */
    /* }}} */

    /* transitions ------------------------------------{{{ */
    /* }}} */
}

