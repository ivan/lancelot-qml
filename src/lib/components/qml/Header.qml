/*   vim:set foldmethod=marker:
 *
 *   Copyright (C) 2013 Ivan Cukic <ivan.cukic(at)kde.org>
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License version 2,
 *   or (at your option) any later version, as published by the Free
 *   Software Foundation
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details
 *
 *   You should have received a copy of the GNU General Public
 *   License along with this program; if not, write to the
 *   Free Software Foundation, Inc.,
 *   51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

import QtQuick 1.1

import org.kde.plasma.core 0.1 as PlasmaCore
import org.kde.plasma.components 0.1 as PlasmaComponents

Item {
    id: main

    /* property declarations --------------------------{{{ */
    property alias text: labelTitle.text
    property alias horizontalAlignment: labelTitle.horizontalAlignment
    /* }}} */

    /* signal declarations ----------------------------{{{ */
    /* }}} */

    /* JavaScript functions ---------------------------{{{ */
    /* }}} */

    /* object properties ------------------------------{{{ */
    /* horizontalAlignment: Text.Center */
    width:  parent.width
    height: labelTitle.height + background.margins.top  + background.margins.bottom
    /* }}} */

    /* child objects ----------------------------------{{{ */
    PlasmaCore.FrameSvgItem {
        id: background

        anchors.fill: parent
        imagePath: "lancelot/action-list-view-headers"

        height: parent.height
        width:  parent.width
    }

    PlasmaComponents.Label {
        id: labelTitle

        x: Math.min(background.margins.left, 8)
        y: background.margins.top

        width: main.width - background.margins.left - background.margins.right
    }

    /* }}} */

    /* states -----------------------------------------{{{ */
    /* }}} */

    /* transitions ------------------------------------{{{ */
    /* }}} */
}

