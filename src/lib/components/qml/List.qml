/*   vim:set foldmethod=marker:
 *
 *   Copyright (C) 2013 Ivan Cukic <ivan.cukic(at)kde.org>
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License version 2,
 *   or (at your option) any later version, as published by the Free
 *   Software Foundation
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details
 *
 *   You should have received a copy of the GNU General Public
 *   License along with this program; if not, write to the
 *   Free Software Foundation, Inc.,
 *   51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

import QtQuick 1.1

import org.kde.plasma.core 0.1 as PlasmaCore

Column {
    id: main

    /* property declarations --------------------------{{{ */
    property QtObject  model
    property alias     title: labelTitle.text
    property bool      showHeader: true

    property alias     delegate: repeater.delegate

    property Item highlight
    /* }}} */

    /* signal declarations ----------------------------{{{ */
    signal activated(variant action)
    /* }}} */

    /* JavaScript functions ---------------------------{{{ */
    /* }}} */

    /* object properties ------------------------------{{{ */
    /* }}} */

    /* child objects ----------------------------------{{{ */
    Header {
        id: labelTitle

        width: main.width

        horizontalAlignment: Text.Center
        visible: main.showHeader

        onTextChanged: {
            visible = main.showHeader && !(labelTitle.text.isEmpty || main.model.count == 0)
        }

        Connections {
            target: main.model
            onCountChanged: {
                labelTitle.visible = main.showHeader && !(labelTitle.text.isEmpty || main.model.count == 0)
                main.highlight.height = 0
            }
        }

        opacity: visible ? 0.5 : 0
    }

    Repeater {
        id: repeater

        model: main.model

        delegate: ListItem {
            id: itemDelegate

            width: main.width

            title:       model.name
            description: model.description ? model.description : ""
            iconSource:  model.icon

            onClicked: {
                main.model.activate(model.action)
                main.activated(model.action)
            }

            onHoveredChanged: if (hovered) {
                highlight.x = x + parent.x
                highlight.y = y + parent.y
                highlight.height = height
            }
        }
    }

    /* }}} */

    /* states -----------------------------------------{{{ */
    /* }}} */

    /* transitions ------------------------------------{{{ */
    /* }}} */
}

