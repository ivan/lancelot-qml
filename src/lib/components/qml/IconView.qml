/*   vim:set foldmethod=marker:
 *
 *   Copyright (C) 2013 Ivan Cukic <ivan.cukic(at)kde.org>
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License version 2,
 *   or (at your option) any later version, as published by the Free
 *   Software Foundation
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details
 *
 *   You should have received a copy of the GNU General Public
 *   License along with this program; if not, write to the
 *   Free Software Foundation, Inc.,
 *   51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

import QtQuick 1.1

import org.kde.plasma.components 0.1 as PlasmaComponents

Flickable {
    id: main

    /* property declarations --------------------------{{{ */
    property list<QtObject> modelsList

    property Component delegate: IconItem {
                id: itemDelegate

                // width: root.width

                title:       model.name
                description: model.description ? model.description : ""
                iconSource:  model.icon

                onClicked: main.model.activate(model.action)

                onHoveredChanged: if (hovered) {
                    highlight.x      = x + main.x
                    highlight.y      = y + main.y + labelTitle.height // + labelTitle.visible ? labelTitle.height : 0
                    highlight.width  = width
                    highlight.height = height
                }
            }
    /* }}} */

    /* signal declarations ----------------------------{{{ */
    /* }}} */

    /* JavaScript functions ---------------------------{{{ */
    /* }}} */

    /* object properties ------------------------------{{{ */
    clip: true
    flickableDirection: Flickable.VerticalFlick

    contentWidth:  content.width
    contentHeight: content.height
    /* }}} */

    /* child objects ----------------------------------{{{ */
    PlasmaComponents.Highlight {
        id: itemHighlight

        width:  0
        height: 0
        x:      0
        y:      0

        visible: (width * height > 0)

        Behavior on x      { NumberAnimation { duration: 200 } }
        Behavior on y      { NumberAnimation { duration: 200 } }
        Behavior on width  { NumberAnimation { duration: 200 } }
        Behavior on height { NumberAnimation { duration: 200 } }
    }

    Column {
        id: content

        width: main.width

        Repeater {
            model: modelsList

            Icons {
                width:  main.width

                delegate: main.delegate

                model:     modelsList[index]
                highlight: itemHighlight

                title:     modelsList[index].selfTitle ? modelsList[index].selfTitle : ""
            }
        }
    }
    /* }}} */

    /* states -----------------------------------------{{{ */
    /* }}} */

    /* transitions ------------------------------------{{{ */
    /* }}} */
}

