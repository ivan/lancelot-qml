/*   vim:set foldmethod=marker:
 *
 *   Copyright (C) 2013 Ivan Cukic <ivan.cukic(at)kde.org>
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License version 2,
 *   or (at your option) any later version, as published by the Free
 *   Software Foundation
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details
 *
 *   You should have received a copy of the GNU General Public
 *   License along with this program; if not, write to the
 *   Free Software Foundation, Inc.,
 *   51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

import QtQuick 1.1

import org.kde.plasma.core 0.1 as PlasmaCore
import org.kde.plasma.components 0.1 as PlasmaComponents

Column {
    id: main

    /* property declarations --------------------------{{{ */
    property alias   title       : labelTitle.text
    property alias   description : labelDescription.text
    //property alias   iconSource  : iconItem.source
    property int     iconSize    : 32
    property bool    active      : false

    property variant iconSource
    /* }}} */

    /* signal declarations ----------------------------{{{ */
    /* }}} */

    /* JavaScript functions ---------------------------{{{ */
    onIconSourceChanged: {
        if (iconSource.toString().indexOf("http") == 0) {
            iconOnline.source = iconSource.toString();
            iconItem.visible = false;

        } else {
            iconItem.source = iconSource
            iconItem.visible = true;
        }

    }
    /* }}} */

    /* object properties ------------------------------{{{ */
    width:  childrenRect.width
    height: childrenRect.height
    /* }}} */

    /* child objects ----------------------------------{{{ */
    PlasmaCore.IconItem {
        id: iconItem

        width:  main.width
        height: main.iconSize

        anchors.horizontalCenter: parent.horizontalCenter

        source: ""
    }

    Item {
        width:  main.width
        height: main.iconSize

        Image {
            id: iconOnline

            anchors.centerIn: parent

            width:  main.iconSize
            height: main.iconSize

            source: ""
        }

        visible: !iconItem.visible
    }

    PlasmaComponents.Label {
        id: labelTitle

        text: "Title"
        elide: Text.ElideRight
        horizontalAlignment: Text.Center

        width: main.width
        anchors.horizontalCenter: parent.horizontalCenter
    }

    Text {
        id: labelDescription

        text: ""
        elide: Text.ElideRight
        horizontalAlignment: Text.Center

        font.pixelSize: labelTitle.font.pixelSize * 4 / 5
        color:          labelTitle.color
        opacity:        main.active ? 1 : 0.5

        width: main.width
        anchors.horizontalCenter: parent.horizontalCenter
    }
    /* }}} */

    /* states -----------------------------------------{{{ */
    /* }}} */

    /* transitions ------------------------------------{{{ */
    /* }}} */
}

