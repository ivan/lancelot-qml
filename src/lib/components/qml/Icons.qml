/*   vim:set foldmethod=marker:
 *
 *   Copyright (C) 2013 Ivan Cukic <ivan.cukic(at)kde.org>
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License version 2,
 *   or (at your option) any later version, as published by the Free
 *   Software Foundation
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details
 *
 *   You should have received a copy of the GNU General Public
 *   License along with this program; if not, write to the
 *   Free Software Foundation, Inc.,
 *   51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

import QtQuick 1.1

Item {
    id: main

    /* property declarations --------------------------{{{ */
    property QtObject model: null
    property QtObject highlight: null
    property alias title: labelTitle.text

    property alias delegate: repeater.delegate
    /* }}} */

    /* signal declarations ----------------------------{{{ */
    /* }}} */

    /* JavaScript functions ---------------------------{{{ */
    /* }}} */

    /* object properties ------------------------------{{{ */
    height: labelTitle.height + flower.childrenRect.height
    /* }}} */

    /* child objects ----------------------------------{{{ */
    Header {
        id: labelTitle

        width: Math.floor(main.width / 112) * 112 // 128 - 2 * 8 = 112

        visible: !title.isEmpty
        horizontalAlignment: Text.Center
    }

    Flow {
        id: flower
        width: parent.width

        anchors {
            topMargin: labelTitle.height + 8
            /*fill: parent*/
            left:  parent.left
            right: parent.right
            top:   parent.top
        }

        Repeater {
            id: repeater

            model: main.model

            delegate: IconItem {
                id: itemDelegate

                // width: root.width

                title:       model.name
                description: model.description ? model.description : ""
                iconSource:  model.icon

                onClicked: main.model.activate(model.action)

                onHoveredChanged: if (hovered) {
                    highlight.x      = x + main.x
                    highlight.y      = y + main.y + labelTitle.height // + labelTitle.visible ? labelTitle.height : 0
                    highlight.width  = width
                    highlight.height = height
                }
            }
        }
    }
    /* }}} */

    /* states -----------------------------------------{{{ */
    /* }}} */

    /* transitions ------------------------------------{{{ */
    /* }}} */
}

